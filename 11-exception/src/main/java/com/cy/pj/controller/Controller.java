package com.cy.pj.controller;

import com.cy.pj.pojo.Result;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @RequestMapping("do03/{n1}/{n2}")
    public Result do01(@PathVariable Integer n1, @PathVariable Integer n2){
        Integer result=n1/n2;
        Result r = new Result("计算结果" + result);
        r.setData(result);
        return r;
    }

}
