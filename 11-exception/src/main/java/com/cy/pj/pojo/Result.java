package com.cy.pj.pojo;
/**
 * 基于此对象封装服务端响应到客户端的数据
 * 状态，对应消息，具体数据
 */

public class Result {
    private Integer state=1;
    private String message="ok";
    private  Object data;

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Result(){}
    public  Result(String message){
        this.message=message;
    }
    public  Result(Object data){
        this.data=data;
    }
    public  Result(Throwable e){
        this.state=0;
        this.message=e.getMessage();
    }
}
