package com.cy.pj.web;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobaExceptionHandle {
    @ExceptionHandler(ArithmeticException.class)
    public  String do02(ArithmeticException e){
        e.printStackTrace();
        return "计算中出现异常"+e.getMessage();
    }
}
