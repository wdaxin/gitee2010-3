package com.cy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
@ResponseBody
@Controller
public class ExceptionController {
    @RequestMapping("do01/{n1}/{n2}")
    public Integer do01(@PathVariable Integer n1, @PathVariable Integer n2){
        Integer result=n1/n2;
        return result;
    }
//    @ExceptionHandler(ArithmeticException.class)
//    public String do02(ArithmeticException e){
//        e.printStackTrace();
//        return "计算异常"+e.getMessage();
//    }


}
