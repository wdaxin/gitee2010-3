package com.cy.pj.goods.pojo.dao;

import java.util.List;
import java.util.Map;

public interface GoodsDao {

    List<Map<String,Object>> findGoods();
}
