package com.cy.pj.common.cache;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;

@SpringBootTest
public class CacheTest {

    @Autowired
    @Qualifier("softCache")//配合@Autowired 使用，不可单独使用
    public Cache cache;

    @Test

    void test(){
        System.out.println(cache);
    }
}
