package com.cy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@SpringBootApplication
public class DbpmsApplication {

    public static void main(String[] args) {
        SpringApplication.run(DbpmsApplication.class, args);
    }

}
