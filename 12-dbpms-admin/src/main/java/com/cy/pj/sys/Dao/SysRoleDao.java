package com.cy.pj.sys.dao;

import com.cy.pj.common.pojo.CheckBox;
import com.cy.pj.sys.pojo.SysRole;
import com.cy.pj.sys.pojo.SysRoleMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SysRoleDao {
    int getRowCount(String name);
    List<SysRole> findPageObjects(String name,
                                  Integer startIndex,
                                  Integer pageSize);

    int insertObject(SysRole entity);
    int updateObject(SysRole entity);
    SysRoleMenu findById(Integer id);

//添加查询角色基本信息的方法,关键代码如下
    @Select("select id,name from sys_roles")
    List<CheckBox> findRoles();


}
