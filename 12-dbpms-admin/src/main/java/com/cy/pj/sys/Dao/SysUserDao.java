package com.cy.pj.sys.dao;



import com.cy.pj.sys.pojo.SysUser;
import org.apache.ibatis.annotations.Mapper;

import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface SysUserDao {

    List<SysUser> findPageObjects(String username);

    //在SysUserDao中定义禁用启动方法及SQL映射,关键代码实现:
    @Update("update sys_users set valid=#{valid},modifiedTime=now() where id=#{id}")
    int validById(Integer id,Integer valid);

    //添加用户自身信息的方法,关键代码如下:
    int insertObject(SysUser entity);

    //在SysUserDao中添加基于id查询用户以及对应部门的方法
    SysUser findById(Integer id);

}
