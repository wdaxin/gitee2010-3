package com.cy.pj.sys.service;

import com.cy.pj.common.pojo.CheckBox;
import com.cy.pj.common.pojo.PageObject;
import com.cy.pj.sys.pojo.SysRole;
import com.cy.pj.sys.pojo.SysRoleMenu;

import java.util.List;

public interface SysRoleService {
    PageObject<SysRole> findPageObjects(String name,
                                        Integer pageCurrent);


    int saveObjects(SysRole entity,Integer[] menuIds);
    SysRoleMenu findById(Integer id);
    int updateObject(SysRole entity,Integer[] menuIds);

    List<CheckBox> findRoles();
}
