package com.cy.pj.sys.controller;

import com.cy.pj.common.pojo.JsonResult;
import com.cy.pj.sys.pojo.SysUser;
import com.cy.pj.sys.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user/")
public class SysUserController {
    @Autowired
    private SysUserService sysUserService;

    @GetMapping("doFindPageObjects")
    public JsonResult doFindPageObjects
            (String username, Integer pageCurrent) {

        return new JsonResult(sysUserService.findPageObjects
                (username, pageCurrent));
    }

    //添加禁用启用方法,关键代码如下:
    @RequestMapping("doValidById")
    public JsonResult doValidById(Integer id, Integer valid) {
        sysUserService.validById(id, valid);
        return new JsonResult("update ok1e");
    }

    @PostMapping("doSaveObject")
    public JsonResult doSaveObject(SysUser entity, Integer[] roleIds) {
        sysUserService.saveObject(entity, roleIds);
        return new JsonResult("save ok11");
    }

    @GetMapping
            ("doFindById/{id}") //这里的url要与客户端一致
    public JsonResult doFindById
            (@PathVariable Integer id) {
        return new JsonResult
                (sysUserService.findById(id));

    }

}














