package com.cy.pj.sys.dao;

import com.cy.pj.sys.pojo.SysLog;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SysLogDao {
    int getRowCount(String username);
    List<SysLog> findPageObjects(String username,
                                 Integer startIndex,
                                 Integer pageSize);
}
