package com.cy.pj.sys.dao;


import com.cy.pj.sys.pojo.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface SysUserRoleDao {

    int insertObjects(Integer userId,Integer[]roleIds);


    //在SysUserRoleDao中添加基于用户id查询角色id的
    // 方法及映射,关键代码如下:
    @Select("select role_id from sys_user_roles " +
            "where user_id=#{userId}")
    List<Integer> findRoleIdsByUserId(Integer userId);
}
