package com.cy.pj.sys.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class SysRole implements Serializable {

    private static final long serialVersionUID = 8999077804084412719L;
    private Integer id;
    private String name;
    private String note;
    private String createdUser;
    private String modifiedUser;
    private Date createdTime;
    private Date modifiedTime;
}
