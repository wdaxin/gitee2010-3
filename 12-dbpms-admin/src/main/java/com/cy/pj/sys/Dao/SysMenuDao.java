package com.cy.pj.sys.dao;

import com.cy.pj.common.pojo.Node;
import com.cy.pj.sys.pojo.SysMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SysMenuDao {
    List<SysMenu> findObjects();

    @Select("select id,name,parentId from sys_menus")
   List<Node> findZtreeMenuNodes();

    int insertObject(SysMenu entity);
    int updateObject(SysMenu entity);
}
