package com.cy.pj.sys.service;

import com.cy.pj.common.pojo.CheckBox;
import com.cy.pj.common.pojo.PageObject;
import com.cy.pj.sys.pojo.SysUser;

import java.util.List;
import java.util.Map;


public interface SysUserService {

    PageObject<SysUser> findPageObjects
            (String username,
             Integer pageCurrent);
    //添加禁用启用方法,关键代码如下:
    int validById(Integer id,Integer valid);
    //中添加保存用户以及用户关系数据的方法,关键代码如下:
    int saveObject(SysUser entity,Integer[]roleIds);

    //在SysUserService接口中定义基于用户id查询用户相
    // 关信息的方法,关键代码如下:
    Map<String,Object> findById(Integer id);
}
