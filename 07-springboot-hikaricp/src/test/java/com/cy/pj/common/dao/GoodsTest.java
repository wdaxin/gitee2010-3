package com.cy.pj.common.dao;

import com.cy.pj.goods.pojo.dao.GoodsDao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;

@SpringBootTest
public class GoodsTest {
    @Autowired
    private GoodsDao goodsDao;

    @Test
    void test02() throws Exception {
        List<Map<String,Object>> list=goodsDao.findGoods();
        for (Map<String,Object> map:list
             ) {
            System.out.println(map);

        }
    }

}
