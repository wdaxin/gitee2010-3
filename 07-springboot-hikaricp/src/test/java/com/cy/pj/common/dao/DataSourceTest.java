package com.cy.pj.common.dao;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@SpringBootTest
public class DataSourceTest {
    @Autowired
    private DataSource dataSource;
    @Test
    void test01() throws SQLException {
        System.out.println(dataSource.getClass().getName());//查询变量的具体信息

        Connection conn=dataSource.getConnection();

        System.out.println(conn);
    }

}
