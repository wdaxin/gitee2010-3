package com.cy.pj.ajax.controller;

import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class AjaxController {
    //假设这是数据库表(一个map对象对应一行记录)
    private List<Map<String,String>> dbList=new ArrayList<>();
    private AjaxController(){
        Map<String,String> map=new HashMap<>();//这个map中存储一些城市信息
        map.put("id", "100");
        map.put("city", "beijing");
        dbList.add(map);
        map=new HashMap<>();//这个map中存储一些城市信息
        map.put("id", "101");
        map.put("city", "shanghai");
        dbList.add(map);
    }
    @PutMapping("/doAjaxPut")
    public String doUpdateObject(@RequestParam Map<String,String> paramMap){
        String id=paramMap.get("id");
        Iterator<Map<String,String>> it=dbList.iterator();
        while(it.hasNext()){
            Map<String,String> map=it.next();
            if(map.get("id").equals(id)){
               map.put("city", paramMap.get("city"));
               //....
            }
        }
        return "update ok";
    };
    @DeleteMapping("/doAjaxDelete/{id}")
    public String doDeleteObject(@PathVariable  String id){
           //基于迭代器执行删除操作
          Iterator<Map<String,String>> it=dbList.iterator();
          while(it.hasNext()){
              Map<String,String> map=it.next();
              if(map.get("id").equals(id)){
                 it.remove();//基于迭代器执行删除操作
              }
          }
          return "delete ok";
    }
    //接收post请求的普通表单数据
    @PostMapping("/doAjaxPost")
    public String doSaveObject(@RequestParam Map<String,String> map){
       dbList.add(map);
       return "save ok";
    }
    //接收post请求方式json格式数据
    @PostMapping("/doAjaxPostJSON")
    public String doSaveJsonObject(@RequestBody Map<String,String> map){
        dbList.add(map);
        return "save json ok";
    }

    //多个url映射为一个方法
    @GetMapping(path={"/doAjaxGet/{city}","/doAjaxGet"})
    public List<Map<String,String>> doAjaxGet(@PathVariable(required = false)  String city){

        if(city==null||"".equals(city))return dbList;
        List<Map<String,String>> queryResult=new ArrayList<>();//基于此集合存储查询结果
        for(Map<String,String> map:dbList){
            if(map.get("city").contains(city)){
                queryResult.add(map);
            }
        }
        return queryResult;
    }
//    @RequestMapping("/doAjaxGet")
//    @ResponseBody
//    public List<Map<String,String>> doAjaxGet(){
//        return dbList;
//    }

    //通过ajax方法发送get请求,访问此方法.
    //方法的返回值要在页面上通过局部刷新的方式进行呈现.
    @GetMapping("/doAjaxStart")
    public String doAjaxStart() throws InterruptedException {
        Thread.sleep(3000);
        return "Response Result Of Ajax Get Request 01 ";
    }
}
