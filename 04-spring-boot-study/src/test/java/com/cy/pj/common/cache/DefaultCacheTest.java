package com.cy.pj.common.cache;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class DefaultCacheTest {//is a special Object
    //spring中可以借助 @Autowired描述属性，用于告诉spring这个属性由spring注入
    @Autowired
    private DefaultCache defaultCache;//spring会对齐进行DI操作

    @Test
    void test01(){
        System.out.println(defaultCache.toString());
    }
    @Autowired
    private BeanFactory beanFactory;

    @Test
    void test02(){
        //基于工厂获取bean对象
        //这个bean对象在哪？
        System.out.println(beanFactory.getBean("defaultCache"));
    }


}














