package com.cy.pj.goods.dao;

import com.cy.pj.goods.pojo.Goods;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class GoodsDaoTest {
    @Autowired
    private GoodsDao goodsDao;
    @Test
    void test01(){
        List<Goods> list=goodsDao.findGoods();
        for (Goods good:list
             ) {
            System.out.println(good);
        }
    }
}
