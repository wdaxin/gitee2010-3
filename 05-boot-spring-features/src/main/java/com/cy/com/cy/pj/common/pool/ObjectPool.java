package com.cy.com.cy.pj.common.pool;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

//@Scope("singleton")//默认作用域为单例作用域
//@Scope("prototype")//多例作用域--每次从spring请求时就创建一个新的对象---本身就延迟加载，不需要@Lazy
@Lazy//延迟加载--延迟对象创建，需要时才创建
@Component
public class ObjectPool {
    public ObjectPool(){
        System.out.println("新建对象");
    }
    @PostConstruct//生命周期初始化方法
    public void init(){
        System.out.println("ObjectPool.init");
    }
    @PreDestroy//生命周期销毁方法
    public void destory(){
        System.out.println("ObjectPool.destory");
    }

}
